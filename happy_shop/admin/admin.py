from django.contrib.admin import AdminSite


class HappyShopAdminSite(AdminSite):
    """自定义AdminSite
    """
    site_header = 'HappyShopAdmin'
    
happy_shop_site = HappyShopAdminSite(name='happy_shop_admin')
